use regex;
use std::collections::HashMap;

pub fn run() -> Result<(), Box<dyn std::error::Error>> {
    let f = std::fs::read_to_string("day2.txt")?;
    let mut s: i32 = 0;
    for line in f.split("\n") {
        if line.len() == 0 {
            continue;
        }

        let re = regex::Regex::new(r"Game (?<id>\d+): (?<game>.+)")?;
        let Some(caps) = re.captures(line) else {
            return Err(Box::from("invalid regex"));
        };
        let Some(game) = caps.name("game") else {
            return Err(Box::from("id not captured"));
        };

        let mut m = HashMap::new();
        for trial in game.as_str().split("; ") {
            for group in trial.split(", ") {
                let res: Vec<&str> = group.split(" ").collect();
                if res.len() < 2 {
                    return Err(Box::from("incorrect group"));
                }

                let count = m.entry(String::from(res[1])).or_insert(0);
                *count = std::cmp::max(*count, res[0].parse()?);
            }
        }

        s += m.values().product::<i32>();
    }

    println!("the sum of power is {s}");

    return Ok(());
}
