use std::error::Error;
pub fn part_b() -> Result<(), Box<dyn Error>> {
    let f = std::fs::read_to_string("day1.txt")?;
    let mut s: i32 = 0;
    let nums = vec![
        "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
    ];
    for line in f.split("\n") {
        if line.len() == 0 {
            continue;
        }

        let mut first: i16 = -1;
        'outer: for (i, ch) in line.char_indices() {
            if ch.is_numeric() {
                first = ch as i16 - '0' as i16;
                break;
            }

            for (j, v) in nums.iter().enumerate() {
                if line[i..].starts_with(v) {
                    first = j as i16;
                    break 'outer;
                }
            }
        }

        let mut last: i16 = -1;
        'outer: for (i, ch) in line.char_indices().rev() {
            if ch.is_numeric() {
                last = ch as i16 - '0' as i16;
                break;
            }

            for (j, v) in nums.iter().enumerate() {
                if line[..i + ch.len_utf8()].ends_with(v) {
                    last = j as i16;
                    break 'outer;
                }
            }
        }

        s += last as i32 + 10 * first as i32;
    }

    println!("the sum is {}", s);

    return Ok(());
}
