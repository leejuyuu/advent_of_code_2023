#[derive(Clone, Copy)]
enum Dir {
    Up,
    Down,
    Right,
    Left,
    Start,
}

struct Pos {
    r: usize,
    c: usize,
    dir: Dir,
}

struct Map {
    map: Vec<Vec<char>>,
}

impl Map {
    fn new(data: String) -> Map {
        let map: Vec<Vec<char>> = data
            .split('\n')
            .filter(|x| x.len() > 0)
            .map(|line| {
                return line.chars().collect();
            })
            .collect();

        return Map { map };
    }

    fn find_start(&self) -> Option<Pos> {
        for (r, row) in self.map.iter().enumerate() {
            for (c, col) in row.iter().enumerate() {
                if *col == 'S' {
                    return Some(Pos {
                        r,
                        c,
                        dir: Dir::Start,
                    });
                }
            }
        }
        return None;
    }

    fn find_next(&self, p: &Pos) -> Result<Pos, String> {
        let ch = self.get(p)?;

        if ch == 'S' {
            if let Ok(ch) = self.get(&Pos { c: p.c - 1, ..*p }) {
                if ch == '-' || ch == 'F' || ch == 'L' {
                    return Ok(Pos {
                        c: p.c - 1,
                        dir: Dir::Left,
                        ..*p
                    });
                }
            }

            if let Ok(ch) = self.get(&Pos { c: p.c + 1, ..*p }) {
                if ch == '-' || ch == '7' || ch == 'J' {
                    return Ok(Pos {
                        c: p.c + 1,
                        dir: Dir::Right,
                        ..*p
                    });
                }
            }

            if let Ok(ch) = self.get(&Pos { r: p.r + 1, ..*p }) {
                if ch == '|' || ch == 'L' || ch == 'J' {
                    return Ok(Pos {
                        r: p.r + 1,
                        dir: Dir::Down,
                        ..*p
                    });
                }
            }

            if let Ok(ch) = self.get(&Pos { r: p.r - 1, ..*p }) {
                if ch == '|' || ch == '7' || ch == 'F' {
                    return Ok(Pos {
                        r: p.r - 1,
                        dir: Dir::Up,
                        ..*p
                    });
                }
            }

            return Err("invalid start position".to_string());
        }

        if ch == '|' {
            match p.dir {
                Dir::Up => return Ok(Pos { r: p.r - 1, ..*p }),
                Dir::Down => return Ok(Pos { r: p.r + 1, ..*p }),
                _ => return Err("invalid direction".to_string()),
            }
        } else if ch == '-' {
            match p.dir {
                Dir::Left => return Ok(Pos { c: p.c - 1, ..*p }),
                Dir::Right => return Ok(Pos { c: p.c + 1, ..*p }),
                _ => return Err("invalid direction".to_string()),
            }
        } else if ch == 'F' {
            match p.dir {
                Dir::Up => {
                    return Ok(Pos {
                        c: p.c + 1,
                        dir: Dir::Right,
                        ..*p
                    })
                }
                Dir::Left => {
                    return Ok(Pos {
                        r: p.r + 1,
                        dir: Dir::Down,
                        ..*p
                    })
                }
                _ => return Err("invalid direction".to_string()),
            }
        } else if ch == '7' {
            match p.dir {
                Dir::Up => {
                    return Ok(Pos {
                        c: p.c - 1,
                        dir: Dir::Left,
                        ..*p
                    })
                }
                Dir::Right => {
                    return Ok(Pos {
                        r: p.r + 1,
                        dir: Dir::Down,
                        ..*p
                    })
                }
                _ => return Err("invalid direction".to_string()),
            }
        } else if ch == 'J' {
            match p.dir {
                Dir::Down => {
                    return Ok(Pos {
                        c: p.c - 1,
                        dir: Dir::Left,
                        ..*p
                    })
                }
                Dir::Right => {
                    return Ok(Pos {
                        r: p.r - 1,
                        dir: Dir::Up,
                        ..*p
                    })
                }
                _ => return Err("invalid direction".to_string()),
            }
        } else if ch == 'L' {
            match p.dir {
                Dir::Down => {
                    return Ok(Pos {
                        c: p.c + 1,
                        dir: Dir::Right,
                        ..*p
                    })
                }
                Dir::Left => {
                    return Ok(Pos {
                        r: p.r - 1,
                        dir: Dir::Up,
                        ..*p
                    })
                }
                _ => return Err("invalid direction".to_string()),
            }
        }

        return Err("cannot find next".to_string());
    }

    fn pos_is_start(&self, p: &Pos) -> Result<bool, String> {
        let ch = self.get(p)?;
        return Ok(ch == 'S');
    }

    fn get(&self, p: &Pos) -> Result<char, String> {
        match self.map.get(p.r) {
            Some(row) => match row.get(p.c) {
                Some(ch) => {
                    return Ok(*ch);
                }
                None => return Err(format!("column {} is out of bounds", p.c)),
            },
            None => return Err(format!("row {} is out of bounds", p.r)),
        }
    }
}

pub fn run() -> Result<(), Box<dyn std::error::Error>> {
    let f = std::fs::read_to_string("day10.txt")?;
    let map = Map::new(f);

    let Some(mut curr) = map.find_start() else {
        return Err(Box::from("cannot find start"));
    };

    let mut winding: Vec<Vec<(usize, char)>> = vec![Vec::new(); map.map.len()];

    let mut start_dir = Dir::Start;
    loop {
        curr = map.find_next(&curr)?;
        if let Dir::Start = start_dir {
            start_dir = curr.dir;
        }
        let ch = map.get(&curr)?;
        if ch != '-' && ch != 'S' {
            winding.get_mut(curr.r).unwrap().push((curr.c, ch));
        }
        if map.pos_is_start(&curr)? {
            match (start_dir, curr.dir) {
                (Dir::Up, Dir::Up) | (Dir::Down, Dir::Down) => {
                    winding.get_mut(curr.r).unwrap().push((curr.c, '|'));
                }
                (Dir::Right, Dir::Up) | (Dir::Down, Dir::Left) => {
                    winding.get_mut(curr.r).unwrap().push((curr.c, 'F'));
                }
                (Dir::Right, Dir::Down) | (Dir::Up, Dir::Left) => {
                    winding.get_mut(curr.r).unwrap().push((curr.c, 'L'));
                }
                (Dir::Left, Dir::Up) | (Dir::Down, Dir::Right) => {
                    winding.get_mut(curr.r).unwrap().push((curr.c, '7'));
                }
                (Dir::Left, Dir::Down) | (Dir::Up, Dir::Right) => {
                    winding.get_mut(curr.r).unwrap().push((curr.c, 'J'));
                }
                _ => {}
            }
            break;
        }
    }

    let res: usize = winding
        .iter_mut()
        .map(|x| {
            x.sort();
            let mut sum: usize = 0;
            let mut last: Option<usize> = None;
            let mut i: usize = 0;
            while i < x.len() {
                let (idx, ch) = x.get(i).unwrap();
                let next = x.get(i + 1);

                if let Some(last) = last {
                    sum += idx - last - 1
                }

                if *ch == 'F' || *ch == 'L' {
                    i += 2;
                } else {
                    i += 1;
                }

                // Left and right is the same side
                if (*ch == 'F' && next.unwrap().1 == '7') || (*ch == 'L' && next.unwrap().1 == 'J')
                {
                    if last.is_some() {
                        last = Some(next.unwrap().0)
                    }
                }

                if *ch == '|' {
                    if last.is_some() {
                        last = None
                    } else {
                        last = Some(*idx);
                    }
                }

                if (*ch == 'F' && next.unwrap().1 == 'J') || (*ch == 'L' && next.unwrap().1 == '7')
                {
                    if last.is_some() {
                        last = None
                    } else {
                        last = Some(next.unwrap().0);
                    }
                }
            }

            return sum;
        })
        .sum();

    println!("the number of tiles is {res}");

    return Ok(());
}
