pub fn run() -> Result<(), Box<dyn std::error::Error>> {
    let f = std::fs::read_to_string("day9.txt")?;

    let res: i32 = f
        .split("\n")
        .filter(|x| x.len() > 0)
        .map(|line| {
            let mut seq: Vec<i32> = line
                .split(' ')
                .map(|s| s.parse::<i32>())
                .flat_map(|x| x)
                .collect();
            let mut seq = &mut seq[..];
            let mut next = 0;
            let mut step = 0;
            while seq.iter().any(|x| *x != 0) {
                let mul = if step % 2 == 0 { 1 } else { -1 };
                next += seq.first().unwrap_or(&0) * mul;
                let len = seq.len();
                (0..len - 1).for_each(|i| {
                    seq[len - 1 - i] -= seq[len - 2 - i];
                });

                seq = &mut seq[1..];
                step += 1;
            }

            return next;
        })
        .sum();

    println!("the sum is {res}");

    return Ok(());
}
