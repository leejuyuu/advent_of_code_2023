use regex;
pub fn run() -> Result<(), Box<dyn std::error::Error>> {
    let f = std::fs::read_to_string("day4.txt")?;
    let mut matches: Vec<usize> = Vec::with_capacity(f.split("\n").filter(|x| x.len() > 0).count());
    for line in f.split("\n").filter(|x| x.len() > 0) {
        let re = regex::Regex::new(r"Card\s+(?<id>\d+): (?<p1>[\d\s]+) \| (?<p2>[\d\s]+)")?;
        let Some(cap) = re.captures(line) else {
            return Err(Box::from("failed to do regex"));
        };
        let Some(winning) = cap.name("p1") else {
            return Err(Box::from("failed to capture winning num"));
        };
        let Some(have) = cap.name("p2") else {
            return Err(Box::from("failed to capture have num"));
        };

        let winning = winning
            .as_str()
            .split(' ')
            .filter(|x| x.len() > 0)
            .filter_map(|x| x.parse::<u32>().ok());
        let have = have
            .as_str()
            .split(' ')
            .filter(|x| x.len() > 0)
            .filter_map(|x| x.parse::<u32>().ok())
            .collect::<Vec<u32>>();

        matches.push(winning.filter(|x| have.contains(x)).count());
    }

    let mut counts: Vec<usize> = vec![1; matches.len()];
    for (i, v) in matches.iter().enumerate() {
        if *v == 0 {
            continue;
        }

        for j in i + 1..i + 1 + v {
            counts[j] += counts[i]
        }
    }

    println!("the score is {}", counts.iter().sum::<usize>());

    return Ok(());
}
