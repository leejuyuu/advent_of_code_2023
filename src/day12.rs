use std::collections::HashMap;

struct Record {
    rec: Vec<char>,
    damaged_counts: Vec<usize>,
}

impl Record {
    fn num_arrangements(&self) -> usize {
        let mut cache = HashMap::new();
        let mut positions: HashMap<usize, Vec<usize>> = HashMap::new();

        for c in self.damaged_counts.iter() {
            if positions.contains_key(&c) {
                continue;
            }

            let mut p = Vec::new();
            let mut i = 0;
            while i < self.rec.len() - c + 1 {
                if self.rec[i..i + c].iter().any(|ch| *ch == '.') {
                    i += 1;
                    continue;
                }

                if i != self.rec.len() - c && self.rec[i + c] == '#' {
                    i += 1;
                    continue;
                }

                if i != 0 && self.rec[i - 1] == '#' {
                    i += 1;
                    continue;
                }

                p.push(i);
                if i != self.rec.len() - c && self.rec[i + c] == '.' {
                    i += c + 1;
                } else {
                    i += 1;
                }
            }

            positions.insert(*c, p);
        }
        // println!("{:?}", positions);

        return (0..self.rec.len())
            .map(|i| self.backtrack(i, 0, &mut cache, &positions))
            .sum();
    }

    // possible arrangements that has the count at count_i placed exactly at rec_i
    fn backtrack(
        &self,
        rec_i: usize,
        count_i: usize,
        cache: &mut HashMap<(usize, usize), usize>,
        positions: &HashMap<usize, Vec<usize>>,
    ) -> usize {
        if let Some(res) = cache.get(&(rec_i, count_i)) {
            return *res;
        }

        // Any # before the first makes this result invalid
        if count_i == 0 {
            if self.rec[..rec_i].iter().any(|x| *x == '#') {
                cache.insert((rec_i, count_i), 0);
                return 0;
            }
        }

        let Some(c) = self.damaged_counts.get(count_i) else {
            cache.insert((rec_i, count_i), 0);
            return 0;
        };

        let Some(p) = positions.get(c) else { return 0 };

        if let Err(_) = p.binary_search(&rec_i) {
            cache.insert((rec_i, count_i), 0);
            return 0;
        };

        if count_i == self.damaged_counts.len() - 1 {
            // Any # after the last makes this result invalid
            if self.rec[rec_i + c..].iter().any(|x| *x == '#') {
                cache.insert((rec_i, count_i), 0);
                return 0;
            }
            cache.insert((rec_i, count_i), 1);
            return 1;
        }

        let mut res = 0;
        for i in rec_i + c + 1..self.rec.len() {
            let re = self.backtrack(i, count_i + 1, cache, positions);
            res += re;

            // Cannot skip any # between contiguous segments
            if self.rec[i] == '#' {
                break;
            }
        }
        cache.insert((rec_i, count_i), res);
        return res;
    }
}

fn repeat_five_times_with_delimiter(input: Vec<char>) -> Vec<char> {
    let n = input.len();
    let mut out = vec!['?'; n * 5 + 4];
    for i in 0..5 {
        out[n * i + i..n * (i + 1) + i].clone_from_slice(&input[..])
    }
    return out;
}

fn repeat_five_times(input: Vec<usize>) -> Vec<usize> {
    let n = input.len();
    let mut out = vec![0; n * 5];
    for i in 0..5 {
        out[n * i..n * (i + 1)].clone_from_slice(&input[..])
    }
    return out;
}

pub fn run() -> Result<(), Box<dyn std::error::Error>> {
    let f = std::fs::read_to_string("day12.txt")?;

    let res: usize = f
        .split('\n')
        .filter(|x| x.len() > 0)
        .map(|line| {
            if let Some((rec, c)) = line.split_once(' ') {
                let rec = repeat_five_times_with_delimiter(rec.chars().collect());
                let damaged_counts =
                    repeat_five_times(c.split(',').flat_map(|x| x.parse::<usize>()).collect());
                return Some(Record {
                    rec,
                    damaged_counts,
                });
            } else {
                return None;
            }
        })
        .flat_map(|v| v)
        .map(|v| v.num_arrangements())
        .sum();
    println!("the number of combinations is {res}");

    return Ok(());
}
