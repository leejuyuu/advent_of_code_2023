pub fn run() -> Result<(), Box<dyn std::error::Error>> {
    let f = std::fs::read_to_string("day6.txt")?;
    let lines: Vec<&str> = f.split('\n').collect();
    let time: i64 = lines[0].replace(" ", "").split(":").collect::<Vec<&str>>()[1].parse()?;
    let dist: i64 = lines[1].replace(" ", "").split(":").collect::<Vec<&str>>()[1].parse()?;

    let res: i64 = num_ways(&time, &dist);
    println!("number of ways is {res}");

    return Ok(());
}

fn num_ways(t: &i64, d: &i64) -> i64 {
    if t * t < 4 * d {
        return 0;
    }

    if t * t == 4 * d && t % 2 == 0 {
        return 1;
    }

    let left = (1..t + 1)
        .find_map(|x| if x * (*t - x) > *d { Some(x) } else { None })
        .unwrap();
    let right = (1..t + 1)
        .rev()
        .find_map(|x| if x * (*t - x) > *d { Some(x) } else { None })
        .unwrap();

    return right - left + 1;
}
