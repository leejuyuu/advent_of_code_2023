#[derive(Clone, Debug)]
struct MapSegment {
    x: u64,
    y: u64,
    len: u64,
    shifted: bool,
}

pub fn run() -> Result<(), Box<dyn std::error::Error>> {
    let f = std::fs::read_to_string("day5.txt")?;
    let f = f.trim();
    let Some((seeds, maps)) = f.split_once("\n\n") else {
        return Err(Box::from("cannot separate seeds and maps"));
    };
    let Some(seeds) = seeds.split_once(": ").and_then(|(_, seeds)| {
        Some(
            seeds
                .split(' ')
                .map(|x| x.parse::<u64>().unwrap())
                .collect::<Vec<u64>>(),
        )
    }) else {
        return Err(Box::from("failed"));
    };

    let mut segs = vec![MapSegment {
        x: 0,
        y: 0,
        len: u64::MAX,
        shifted: false,
    }];
    for m in maps.split("\n\n") {
        let m = &m[m.find("\n").unwrap() + 1..];
        let ms: Vec<MapSegment> = m
            .split("\n")
            .map(|line| {
                let v = line
                    .split(' ')
                    .map(|x| x.parse::<u64>().unwrap())
                    .collect::<Vec<u64>>();
                MapSegment {
                    y: v[0],
                    x: v[1],
                    len: v[2],
                    shifted: false,
                }
            })
            .collect();

        for m in ms {
            let mut new_segs = Vec::new();
            for seg in segs {
                if seg.shifted {
                    new_segs.push(seg.clone());
                    continue;
                }
                let mut clone = seg.clone();

                if seg.y < m.x + m.len && m.x + m.len < seg.y + seg.len {
                    let y = m.x + m.len;
                    new_segs.push(MapSegment {
                        x: y - seg.y + seg.x,
                        y,
                        len: seg.y + seg.len - y,
                        shifted: false,
                    });
                    clone.len = y - seg.y;
                }

                if m.x <= seg.y && seg.y < m.x + m.len {
                    clone.y = if m.y > m.x {
                        seg.y + (m.y - m.x)
                    } else {
                        seg.y - (m.x - m.y)
                    };
                    clone.shifted = true;
                } else if seg.y < m.x && m.x < seg.y + seg.len {
                    new_segs.push(MapSegment {
                        x: m.x - seg.y + seg.x,
                        y: m.y,
                        len: clone.len - (m.x - seg.y),
                        shifted: true,
                    });
                    clone.len = m.x - seg.y;
                }

                new_segs.push(clone)
            }
            segs = new_segs;
        }

        segs.iter_mut().for_each(|x| x.shifted = false);
    }

    let f = |x: &u64| -> u64 {
        if let Some(v) = segs.iter().find_map(|v| {
            if v.x <= *x && *x < v.x + v.len {
                Some(v)
            } else {
                None
            }
        }) {
            return x - v.x + v.y;
        }
        return *x;
    };

    let Some(min) = (0..seeds.len() / 2)
        .map(|i| {
            let start = seeds[i * 2];
            let len = seeds[i * 2 + 1];

            let seg_min = segs
                .iter()
                .filter(|s| start <= s.x && s.x < start + len)
                .map(|s| s.y)
                .min();
            let start_y = f(&start);
            match seg_min {
                Some(min) => start_y.min(min),
                None => start_y,
            }
        })
        .min()
    else {
        return Err(Box::from("min not found"));
    };
    println!("lowest location is {min}");

    return Ok(());
}
