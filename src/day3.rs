use std::collections::HashMap;
#[derive(Clone, Debug)]
struct Number {
    start: usize,
    len: usize,
    val: u32,
    valid: bool,
}

pub fn run() -> Result<(), Box<dyn std::error::Error>> {
    let f = std::fs::read_to_string("day3.txt")?;

    let mut s = 0;

    let mut symbols: HashMap<usize, Vec<u32>> = HashMap::new();
    let mut numbers: Vec<Number> = Vec::new();
    for line in f.split("\n") {
        let mut new_symbols: HashMap<usize, Vec<u32>> = HashMap::new();
        let mut new_nums = Vec::new();
        let mut curr_num = Number {
            start: 0,
            len: 0,
            val: 0,
            valid: false,
        };
        for (i, ch) in line.chars().enumerate() {
            if curr_num.valid {
                if ch.is_ascii_digit() {
                    curr_num.len += 1;
                    curr_num.val = curr_num.val * 10 + ch.to_digit(10).unwrap();
                    continue;
                }

                curr_num.valid = false;
                new_nums.push(curr_num.clone());
                if ch == '*' {
                    let v = new_symbols.entry(i).or_default();
                    v.push(curr_num.val)
                }
                if curr_num.start > 0 {
                    if let Some(v) = new_symbols.get_mut(&(curr_num.start - 1)) {
                        v.push(curr_num.val);
                    }
                }
                for (x, v) in symbols.iter_mut() {
                    if *x as i32 >= (curr_num.start as i32 - 1)
                        && *x <= curr_num.start + curr_num.len
                    {
                        v.push(curr_num.val);
                    }
                }
            } else {
                if ch.is_ascii_digit() {
                    curr_num.start = i;
                    curr_num.valid = true;
                    curr_num.len = 1;
                    curr_num.val = ch.to_digit(10).unwrap();
                    continue;
                }
            }

            if ch == '*' {
                let sym = new_symbols.entry(i).or_default();
                for v in numbers.iter() {
                    if (v.start as i32 - 1) <= i as i32 && v.start + v.len >= i {
                        sym.push(v.val)
                    }
                }
            }
        }

        // Number at the end of the line
        if curr_num.valid {
            curr_num.valid = false;
            if curr_num.start > 0 {
                if let Some(v) = new_symbols.get_mut(&(curr_num.start - 1)) {
                    v.push(curr_num.val);
                }
            }

            for (x, v) in symbols.iter_mut() {
                if *x as i32 >= (curr_num.start as i32 - 1) && *x <= curr_num.start + curr_num.len {
                    v.push(curr_num.val);
                }
            }
            new_nums.push(curr_num.clone())
        }

        s += symbols
            .values()
            .filter(|v| v.len() == 2)
            .map(|v| v[0] * v[1])
            .sum::<u32>();

        symbols = new_symbols;
        numbers = new_nums;
    }

    println!("the sum is {s}");

    return Ok(());
}
