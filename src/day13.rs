struct Pattern {
    w: usize,
    h: usize,
    data: Vec<Vec<char>>,
}

impl Pattern {
    fn new(s: &str) -> Pattern {
        let data: Vec<Vec<char>> = s
            .split('\n')
            .filter(|x| x.len() > 0)
            .map(|x| x.chars().collect())
            .collect();

        return Pattern {
            data: data.clone(),
            h: data.len(),
            w: data[0].len(),
        };
    }

    fn vertical_reflection(&self) -> usize {
        'outer: for j in 1..self.w {
            let r = std::cmp::min(j, self.w - j);
            let mut c = 0;
            for d in 0..r {
                c += (0..self.h)
                    .filter(|i| self.data[*i][j - 1 - d] != self.data[*i][j + d])
                    .count();
                if c > 1 {
                    continue 'outer;
                }
            }

            if c == 1 {
                return j;
            }
        }

        return 0;
    }

    fn horizontal_reflection(&self) -> usize {
        'outer: for i in 1..self.h {
            let r = std::cmp::min(i, self.h - i);
            let mut c = 0;
            for d in 0..r {
                c += (0..self.w)
                    .filter(|j| self.data[i - 1 - d][*j] != self.data[i + d][*j])
                    .count();
                if c > 1 {
                    continue 'outer;
                }
            }

            if c == 1 {
                return i;
            }
        }

        return 0;
    }
}

pub fn run() -> Result<(), Box<dyn std::error::Error>> {
    let f = std::fs::read_to_string("day13.txt")?;
    let res: usize = f
        .split("\n\n")
        .filter(|x| x.len() > 0)
        .map(|x| Pattern::new(x))
        .map(|x| 100 * x.horizontal_reflection() + x.vertical_reflection())
        .sum();

    println!("the sum is {res}");

    return Ok(());
}
