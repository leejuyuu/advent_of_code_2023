mod day14;
fn main() {
    match day14::run() {
        Ok(_) => (),
        Err(err) => println!("error: {}", err),
    }
}
