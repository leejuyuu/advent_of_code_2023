use std::collections::HashMap;

pub fn run() -> Result<(), Box<dyn std::error::Error>> {
    let f = std::fs::read_to_string("day14.txt")?;
    let mut map: Vec<Vec<char>> = f
        .split('\n')
        .filter(|x| x.len() > 0)
        .map(|x| x.chars().collect())
        .collect();
    let mut cache: HashMap<Vec<Vec<char>>, i32> = HashMap::new();
    const N: i32 = 1000000000;
    for i in 0..N {
        if let Some(last_i) = cache.get(&map) {
            let cycle = i - last_i;
            let final_i = (N - last_i) % cycle;
            for _ in 0..final_i {
                map = roll_cycle(&map);
            }
            break;
        }

        cache.insert(map.clone(), i);
        map = roll_cycle(&map);
    }
    let res = total_load(&map);
    println!("the total load is {res}");
    return Ok(());
}

fn roll_cycle(map: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    let mut new_map = map.clone();
    roll_north(&mut new_map);
    roll_west(&mut new_map);
    roll_south(&mut new_map);
    roll_east(&mut new_map);
    return new_map;
}

fn roll_north(map: &mut Vec<Vec<char>>) {
    let w = map[0].len();
    let h = map.len();
    for j in 0..w {
        let mut write_i = 0;
        for i in 0..h {
            let ch = map[i][j];
            if ch == '#' {
                write_i = i + 1;
            }

            if ch == 'O' {
                (map[write_i][j], map[i][j]) = (map[i][j], map[write_i][j]);
                write_i += 1;
            }
        }
    }
}

fn roll_south(map: &mut Vec<Vec<char>>) {
    let w = map[0].len();
    let h = map.len();
    for j in 0..w {
        let mut write_i = h - 1;
        for i in (0..h).rev() {
            let ch = map[i][j];
            if ch == '#' {
                // Guard against usize overflow
                if i > 0 {
                    write_i = i - 1;
                }
            }

            if ch == 'O' {
                (map[write_i][j], map[i][j]) = (map[i][j], map[write_i][j]);
                if write_i > 0 {
                    write_i -= 1;
                }
            }
        }
    }
}

fn roll_west(map: &mut Vec<Vec<char>>) {
    let w = map[0].len();
    let h = map.len();
    for i in 0..h {
        let mut write_j = 0;
        for j in 0..w {
            let ch = map[i][j];
            if ch == '#' {
                write_j = j + 1;
            }

            if ch == 'O' {
                (map[i][write_j], map[i][j]) = (map[i][j], map[i][write_j]);
                write_j += 1;
            }
        }
    }
}

fn roll_east(map: &mut Vec<Vec<char>>) {
    let w = map[0].len();
    let h = map.len();
    for i in 0..h {
        let mut write_j = w - 1;
        for j in (0..w).rev() {
            let ch = map[i][j];
            if ch == '#' {
                if j > 0 {
                    write_j = j - 1;
                }
            }

            if ch == 'O' {
                (map[i][write_j], map[i][j]) = (map[i][j], map[i][write_j]);
                if write_j > 0 {
                    write_j -= 1;
                }
            }
        }
    }
}

fn total_load(map: &Vec<Vec<char>>) -> usize {
    return map
        .iter()
        .enumerate()
        .map(|(i, row)| (map.len() - i) * row.iter().filter(|ch| **ch == 'O').count())
        .sum();
}
