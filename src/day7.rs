use std::{cmp::Ordering, collections::HashMap, iter::zip};

#[derive(Eq, PartialEq, PartialOrd, Ord, Debug)]
enum Kind {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

#[derive(Debug)]
struct Hand<'a> {
    val: &'a str,
    kind: Kind,
}

impl<'a> Hand<'a> {
    fn new(val: &str) -> Hand {
        let mut m = HashMap::new();
        for ch in val.chars() {
            *(m.entry(ch).or_insert(0)) += 1
        }

        if let Some(num_j) = m.remove(&'J') {
            let k = m
                .iter()
                .max_by(|a, b| a.1.cmp(b.1))
                .map(|(k, _)| k)
                .unwrap_or(&'J');
            *m.entry(*k).or_insert(0) += num_j;
        }

        let kind = if m.len() == 5 {
            Kind::HighCard
        } else if m.len() == 4 {
            Kind::OnePair
        } else if m.len() == 3 && *m.values().max().unwrap() == 2 {
            Kind::TwoPair
        } else if m.len() == 3 {
            Kind::ThreeOfAKind
        } else if m.len() == 2 && *m.values().max().unwrap() == 3 {
            Kind::FullHouse
        } else if m.len() == 2 {
            Kind::FourOfAKind
        } else {
            Kind::FiveOfAKind
        };

        return Hand { val, kind };
    }

    fn cmp(&self, other: &Hand) -> Ordering {
        if self.kind != other.kind {
            return self.kind.cmp(&other.kind);
        }

        for (a, b) in zip(self.val.chars(), other.val.chars()) {
            if a == b {
                continue;
            }
            return cmp_card(a, b);
        }

        return Ordering::Equal;
    }
}

fn cmp_card(a: char, b: char) -> Ordering {
    if a == b {
        return Ordering::Equal;
    }

    if a == 'J' {
        return Ordering::Less;
    }

    if b == 'J' {
        return Ordering::Greater;
    }

    for card in ['A', 'K', 'Q', 'T'] {
        if a == card {
            return Ordering::Greater;
        }

        if b == card {
            return Ordering::Less;
        }
    }
    return a.cmp(&b);
}

#[derive(Debug)]
struct Entry<'a> {
    hand: Hand<'a>,
    bid: usize,
}

pub fn run() -> Result<(), Box<dyn std::error::Error>> {
    let f = std::fs::read_to_string("day7.txt")?;
    let mut entries: Vec<Entry> = f
        .split('\n')
        .filter(|x| x.len() > 0)
        .map(|x| {
            let v: Vec<&str> = x.split(' ').collect();
            Entry {
                hand: Hand::new(v[0]),
                bid: v[1].parse::<usize>().unwrap(),
            }
        })
        .collect();
    entries.sort_by(|a, b| a.hand.cmp(&b.hand));

    let res: usize = entries
        .iter()
        .enumerate()
        .map(|(i, v)| (i + 1) * v.bid)
        .sum();

    println!("the total winnings is {res}");

    return Ok(());
}
