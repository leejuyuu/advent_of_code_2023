use std::collections::HashMap;

#[derive(Debug)]
struct Node<'a> {
    left: &'a str,
    right: &'a str,
}

pub fn run() -> Result<(), Box<dyn std::error::Error>> {
    let f = std::fs::read_to_string("day8.txt")?;
    let Some((inst, g)) = f.split_once("\n\n") else {
        return Err(Box::from("cannot split first line"));
    };

    let mut graph: HashMap<&str, Node> = HashMap::new();
    for line in g.split('\n').filter(|x| x.len() > 0) {
        graph.insert(
            &line[..3],
            Node {
                left: &line[7..10],
                right: &line[12..15],
            },
        );
    }

    let curr: Vec<&&str> = graph.keys().filter(|x| x.ends_with('A')).collect();

    let counts = curr.iter().map(|curr| {
        let mut curr = *curr;
        let mut count: u64 = 0;
        loop {
            for ch in inst.chars() {
                count += 1;
                if ch == 'L' {
                    curr = &graph.get(curr).unwrap().left;
                } else {
                    curr = &graph.get(curr).unwrap().right;
                }

                if curr.ends_with('Z') {
                    return count;
                }
            }
        }
    });

    let count = counts.reduce(|acc, e| lcm(acc, e)).unwrap_or(0);

    println!("requires {count} steps");

    return Ok(());
}

// https://en.wikipedia.org/wiki/Euclidean_algorithm
fn gcd(mut a: u64, mut b: u64) -> u64 {
    return loop {
        (a, b) = (b, a % b);
        if b == 0 {
            break a;
        }
    };
}

fn lcm(a: u64, b: u64) -> u64 {
    return a * b / gcd(a, b);
}
