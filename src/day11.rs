struct Map {
    xs: Vec<usize>,
    ys: Vec<usize>,
}

impl Map {
    fn new(data: String) -> Map {
        let lines: Vec<&str> = data.split('\n').filter(|x| x.len() > 0).collect();
        let mut m = Map {
            xs: vec![0; lines[0].chars().count()],
            ys: vec![0; lines.len()],
        };
        for (i, line) in lines.iter().enumerate() {
            for (j, ch) in line.chars().enumerate() {
                if ch == '#' {
                    m.xs[i] += 1;
                    m.ys[j] += 1;
                }
            }
        }

        return m;
    }

    fn sum_dists(&self) -> usize {
        return sum_dists(&self.xs) + sum_dists(&self.ys);
    }
}

fn expanded(arr: &Vec<usize>) -> Vec<(usize, usize)> {
    let mut curr: usize = 0;
    let mut expanded: Vec<(usize, usize)> = Vec::with_capacity(arr.len());
    for n in arr {
        if *n == 0 {
            curr += 1_000_000;
            continue;
        }
        expanded.push((curr, *n));
        curr += 1;
    }

    return expanded;
}

fn sum_dists(arr: &Vec<usize>) -> usize {
    let total: usize = arr.iter().sum();
    let mut past: usize = 0;
    let mut sum: usize = 0;
    let arr = expanded(arr);
    for i in 0..arr.len() - 1 {
        past += arr[i].1;
        sum += (arr[i + 1].0 - arr[i].0) * (total - past) * past;
    }

    return sum;
}

pub fn run() -> Result<(), Box<dyn std::error::Error>> {
    let f = std::fs::read_to_string("day11.txt")?;

    let m = Map::new(f);

    let res = m.sum_dists();
    println!("the total dists is {res}");

    return Ok(());
}
